// Introduction to DOM (document object model)
// In JavaSCript each element in a webpage such as image text inputs can be considered as object
// Object in JS contains informations about a given object such as proerties.....
// Document = is the whole webpage | querySelector is used to specific object (HTML element) from document



console.log(document);
console.log(document.querySelector("#txt-first-name"));
// the result is element from html
/*
	document - refers to the whole webpage
	querySelector - is used to select a specific element (object) as long as it is insde the HTML tag(HTML element)
	- takes a string inout that is formatted like CSS selector
	- can select elements regardless if the string is an id, a class, or a tag as long as the element is exsiting in the webpage
*/

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


/*
	event - actions that the user is doing in our webpage (scroll, click, hover, keypress/typing)

	addEventListener - is a function that lets the webpage listen to the events performed by the user
		- takes two arguments
			- string - the event to which the HTML element will listen, these are pre-determined
			- function - executed by the element once the event (fisrt argument) is triggerred. 
*/

txtFirstName.addEventListener('keyup', (event)=>{
	spanFullName.innerHTML = txtFirstName.value
})

txtFirstName.addEventListener('keyup',(event)=>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})


txtLastName.addEventListener('keyup', (event)=>{
	spanFullName.innerHTML = txtLastName.value
})

txtLastName.addEventListener('keyup',(event)=>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})